package triskelTable;

import java.util.ArrayList;
import java.util.List;

public class Table {

    private final String[] columns;
    private final List<Long[]> rows;

    public Table(String[] columns) {
        this.columns = columns;
        this.rows = new ArrayList<>();
    }

    public int size() {
        return rows.size();
    }

    public String[] getColumns() {
        return columns;
    }

    public Table join(Table table) {
        String commonColumn = getCommonColumn(this, table);
        if (commonColumn == null) return null;
        Table resultTable= new Table(createJoinColumnHeader(this, table));
        int rowNumber = 0;
        for (int i = 0; i < this.rows.size(); i++) {
            Long[] row = getRow(i);
            rowNumber = table.existValue(row[index(commonColumn)], commonColumn);
            if (rowNumber != -1){
                resultTable.addRow(createJoinRow(row, table.getRow(rowNumber), table.index(commonColumn)));
            }
        }
        return resultTable;
    }

    public Table select(String[] columns) {
        if ((columns == null) || (columns.length == 0)) return null;
        Table result = new Table(columns);
        for (int i = 0; i < this.rows.size(); i++) {
            Long[] row = new Long[columns.length];
            for (int j = 0; j < columns.length; j++) {
                row[j] = get(i, columns[j]);
            }
            result.addRow(row);
        }
        return result;
    }

    public void addRow(Long[] ids) {
        if (ids.length != columns.length) {
            return;
        }
        rows.add(ids);
    }

    public Long[] getRow(int index) {
        return rows.get(index);
    }

    public Long get(int row, int column) {
        return rows.get(row)[column];
    }

    public Long get(int row, String column) {
        return get(row, index(column));
    }

    private int index(String column) {
        for (int i = 0; i < columns.length; i++) {
            if (columns[i].equalsIgnoreCase(column)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Table) {
            return equals((Table) obj);
        }
        return false;
    }

    private boolean equals(Table table) {
        if (this.columns.length != table.columns.length) {
            return false;
        }
        if (this.rows.size() != table.size()) {
            return false;
        }
        return compareTables(table);
    }

    private boolean compareTables(Table table) {
        if (!compareColumns(table))return false;
        if (!compareRows(table)) return false;
        return true;
    }

    private boolean compareColumns(Table table) {
        boolean coincidence = false;
        for (String column : this.columns) {
            for (String tableColumn : table.columns){
                if (column.equals(tableColumn)){
                    coincidence = true;
                    break;
                }
            }
            if (coincidence) coincidence = false;
            else return false;
        }
        return true;
    }

    private boolean compareRows(Table table) {
        boolean coincidence = false;
        for (Long[] row : this.rows){
            for (Long[] tableRow : table.rows) {
                if (compareValues(row, tableRow)){
                    coincidence = true;
                    break;
                }
            }
            if (coincidence) coincidence = false;
            else return false;
        }
        return true;
    }

    private boolean compareValues(Long[] row, Long[] tableRow) {
        boolean coincidence = false;
        for (Long value : row) {
            for (Long tableValue : tableRow) {
                if (value.equals(tableValue)){
                    coincidence = true;
                    break;
                }
            }
            if (coincidence) coincidence = false;
            else return false;
        }
        return true;
    }

    ////////////// JOIN AUX FUNCTION
    
    private String getCommonColumn(Table firstTable, Table secondTable) {
        for (String firstTableColumn : firstTable.columns) {
            for (String secondTableColumn : secondTable.columns) {
                if (firstTableColumn.equals(secondTableColumn))return secondTableColumn;
            }
        }
        return null;
    }

    private String[] createJoinColumnHeader(Table firstTable, Table secondTable) {
        String[] resultColumnHeader = new String[firstTable.columns.length + secondTable.columns.length -1];
        int count = 0;
        String commonColumn = getCommonColumn(firstTable, secondTable);
        for (String column : firstTable.columns) {
            resultColumnHeader[count] = column;
            count++;
        }
        for (String column : secondTable.columns){
            if (!column.equals(commonColumn)){
            resultColumnHeader[count] = column;
            count++;
            }
        }
        return resultColumnHeader;
    }
    
    private Long[] createJoinRow(Long[] rowA, Long[] rowB, int commonColumnIndex){
        Long[] resultRow = new Long[rowA.length + rowB.length -1];
        int count = 0;
        for (int i = 0; i < rowA.length; i++) {
            resultRow[count] = rowA[i];
            count++;
        }
        for (int i = 0; i < rowB.length; i++){
            if (i != commonColumnIndex){
            resultRow[count] = rowB[i];
            count++;
            }
        }
        return resultRow;
    }

    private int existValue(Long value, String column) {
        Long[] row;
        for (int i = 0; i < this.rows.size(); i++) {
            row = getRow(i);
            if (row[index(column)].equals(value))return i;
        }
        return -1;
    }

}
