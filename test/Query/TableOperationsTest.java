package Query;

import triskelTable.Table;
import org.junit.Test;
import static org.junit.Assert.*;

public class TableOperationsTest extends TableSpace {

    ///////////////////////////////////////////////////
    // Equals /////////////////////////////////////////
    ///////////////////////////////////////////////////
    @Test
    public void equalsTable() {
        Table result = createAuthorTable();
        assertEquals(createAuthorTable(), result);
        assertFalse(createTitleTable().equals(result));
    }

    @Test
    public void equalsTableColumnsNotInOrder() {
        assertEquals(createAuthorTitleTable(), createTitleAuthorTable());
    }

    @Test
    public void equalsTableRowsNotInOrder() {
        assertEquals(createAuthorTitleTable(), createAuthorTitleV2Table());
    }

    @Test
    public void equalsTableNotInSameOrder() {
        assertEquals(createAuthorTitleTable(), createTitleAuthorV2Table());
    }

    ///////////////////////////////////////////////////
    /// Join //////////////////////////////////////////
    ///////////////////////////////////////////////////
    @Test
    public void joinTable() {
        Table authorResult = createAuthorTable();
        Table titleResult = createTitleTable();
        Table join = authorResult.join(titleResult);
        assertTrue(createJoinTable().equals(join));
    }
    
    @Test
    public void joinTableNoCommonColumn(){
        Table authorResult = createAuthorTable();
        Table titleResult = createTitleV2Table();
        Table join = authorResult.join(titleResult);
        assertTrue(join == null);
    }
    
    @Test
    public void joinTableNotInOrder(){
        Table authorResult = createAuthorV2Table();
        Table titleResult = createTitleTable();
        Table join = authorResult.join(titleResult);
        assertTrue(createJoinTable().equals(join));
    }
    
    @Test
    public void joinTableWithNoValues(){
        Table isbn = new Table(new String[]{"a","ISBN"});
        Table price = new Table(new String[]{"a","price"});
        Table join = isbn.join(price);
        assertTrue(new Table(new String[]{"a","ISBN","price"}).equals(join));
    }
    
    @Test
    public void joinTableOneWithNoValues(){
        Table titleResult = createTitleTable();
        Table price = new Table(new String[]{"a","price"});
        Table join = titleResult.join(price);
        assertTrue(new Table(new String[]{"a","title","price"}).equals(join));
    }

    ///////////////////////////////////////////////////
    /// Select ////////////////////////////////////////
    ///////////////////////////////////////////////////
    @Test
    public void selectOneColumnTable() {
        Table join = createJoinTable();
        Table expected = createUniqueColumnAuthor();
        assertEquals(expected, join.select(new String[]{"author"}));
    }

    @Test
    public void selectTwoColumnsTable() {
        Table join = createJoinTable();
        Table expected = createAuthorTitleTable();
        assertEquals(expected, join.select(new String[]{"author", "title"}));
    }

    @Test
    public void selectNoColumns() {
        Table join = createJoinTable();
        assertTrue(join.select(null) == null);
    }

    @Test
    public void selectEmptyColumns() {
        Table join = createJoinTable();
        assertTrue(join.select(new String[]{}) == null);
    }

    ///////////////////////////////////////////////////
    /// General ///////////////////////////////////////
    ///////////////////////////////////////////////////        
    @Test
    public void singleColumnTable() {
        Table result = new Table(new String[]{"a"});
        result.addRow(new Long[]{1l});
        result.addRow(new Long[]{2l});
        result.addRow(new Long[]{5l});
        result.addRow(new Long[]{8l});
        assertEquals(4, result.size());
        assertEquals(new Long(1), result.getRow(0)[0]);
        assertEquals(new Long(1), result.get(0, 0));
        assertEquals(new Long(1), result.get(0, "a"));
        assertEquals(new Long(8), result.get(3, "a"));
    }

    @Test
    public void doubleColumnTable() {
        Table result = createAuthorTable();
        assertEquals(6, result.size());
        assertEquals(new Long(1), result.getRow(0)[0]);
        assertEquals(new Long(1), result.get(0, 0));
        assertEquals(new Long(20), result.get(0, 1));
        assertEquals(new Long(20), result.get(0, "author"));
        assertEquals(new Long(19), result.get(3, "author"));
    }

}
