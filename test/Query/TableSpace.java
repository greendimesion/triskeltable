package Query;

import triskelTable.Table;
import org.junit.Test;

public class TableSpace {

    public Table createAuthorTable() {
        Table result = new Table(new String[]{"a", "author"});
        result.addRow(new Long[]{1l, 20l});
        result.addRow(new Long[]{2l, 30l});
        result.addRow(new Long[]{5l, 80l});
        result.addRow(new Long[]{8l, 19l});
        result.addRow(new Long[]{9l, 39l});
        result.addRow(new Long[]{4l, 91l});
        return result;
    }
    
    public Table createAuthorV2Table() {
        Table result = new Table(new String[]{"author", "a"});
        result.addRow(new Long[]{20l, 1l});
        result.addRow(new Long[]{30l, 2l});
        result.addRow(new Long[]{80l, 5l});
        result.addRow(new Long[]{19l, 8l});
        result.addRow(new Long[]{39l, 9l});
        result.addRow(new Long[]{91l, 4l});
        return result;
    }

    public Table createTitleTable() {
        Table result = new Table(new String[]{"a", "title"});
        result.addRow(new Long[]{1l, -120l});
        result.addRow(new Long[]{2l, -130l});
        result.addRow(new Long[]{8l, -119l});
        result.addRow(new Long[]{4l, -191l});
        result.addRow(new Long[]{7l, -291l});
        return result;
    }
    
    public Table createTitleV2Table() {
        Table result = new Table(new String[]{"b", "title"});
        result.addRow(new Long[]{1l, -120l});
        result.addRow(new Long[]{2l, -130l});
        result.addRow(new Long[]{8l, -119l});
        result.addRow(new Long[]{4l, -191l});
        result.addRow(new Long[]{7l, -291l});
        return result;
    }

    public Table createJoinTable() {
        Table result = new Table(new String[]{"a", "author", "title"});
        result.addRow(new Long[]{1l, 20l, -120l});
        result.addRow(new Long[]{2l, 30l, -130l});
        result.addRow(new Long[]{8l, 19l, -119l});
        result.addRow(new Long[]{4l, 91l, -191l});
        return result;
    }

    public Table createAuthorTitleTable() {
        Table result = new Table(new String[]{"author", "title"});
        result.addRow(new Long[]{20l, -120l});
        result.addRow(new Long[]{30l, -130l});
        result.addRow(new Long[]{19l, -119l});
        result.addRow(new Long[]{91l, -191l});
        return result;
    }
    
    public Table createAuthorTitleV2Table() {
        Table result = new Table(new String[]{"author", "title"});
        result.addRow(new Long[]{20l, -120l});
        result.addRow(new Long[]{19l, -119l});
        result.addRow(new Long[]{30l, -130l});
        result.addRow(new Long[]{91l, -191l});
        return result;
    }

    public Table createTitleAuthorTable() {
        Table result = new Table(new String[]{"title", "author"});
        result.addRow(new Long[]{-120l, 20l});
        result.addRow(new Long[]{-130l, 30l});
        result.addRow(new Long[]{-119l, 19l});
        result.addRow(new Long[]{-191l, 91l});
        return result;
    }
    
    public Table createTitleAuthorV2Table() {
        Table result = new Table(new String[]{"title", "author"});
        result.addRow(new Long[]{-120l, 20l});
        result.addRow(new Long[]{-119l, 19l});
        result.addRow(new Long[]{-130l, 30l});
        result.addRow(new Long[]{-191l, 91l});
        return result;
    }

    public Table createUniqueColumnAuthor(){
        Table result = new Table(new String[]{"author"});
        result.addRow(new Long[]{20l});
        result.addRow(new Long[]{30l});
        result.addRow(new Long[]{19l});
        result.addRow(new Long[]{91l});
        return result;
    }
    
}
